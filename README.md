# Application Challenge

Welcome to this application challenge. Your mission, should you choose to accept
it, is to build a shopping list application using Angular 5. 

## Getting started

To start, you should make a fork of this repo. As long as you have node and npm installed on your computer, you should be able to do the following:

```
ng serve
```

This should build all the code in the application and start a server listening on port 4200. If you open your browser to http://localhost:4200 you should see this:

![screenshot](screenshot.png)

This will mean your environment is set up correctly and you are ready to start coding!

## The Shopping List

We'd like you to build a basic shopping list application. It should have the following features:

1. **Create a shopping list.** A shopping list should have a name and list of items.
2. **Edit a shopping list.** A user should be able to change the name of their shopping list.
3. **Add a shopping list item.** A user should be able to add items to a shopping list. Each item should have a name, SKU #, and price.
4. **Removing an item.** A user should be able to remove an item from a list.

## Criteria

Impress us! We value code that is simple and easy to understand, as well as an application that works. We don't have any expectations that you have a server portion to this application, but if you're able to get these features implemented and want to stretch yourself/impress us more, here are some things you could try:

### Test your code
Ideally we'd do test driven development (TDD), but if your time is constraint, we'd rather you to have a working version of the application to show us.  

### Store the shopping list
There are options like [Firebase](https://www.firebase.com/docs/web/libraries/react/) that would allow you to persist the shopping list in the cloud. That might be fun!

### Add features
Think of other things a shopping list might need to be it more useful.

## Submitting the finished application

Once you've built your application, submit a pull request to us. If you are new to using git there are [many](https://try.github.io/) [online](https://git-scm.com/documentation) [resources](http://gitimmersion.com/) for learning git. It will be necessary to have some basic working knowledge of git to complete this challenge, and it is a core skill you'll be using as a developer.

## Timeframe

**This part is important: I'll be reviewing these as soon as you submit it, I'd expect a submission no longer than 2 weeks.**

We will review and select candidates to do pairing sessions with me on adding a feature to this application either in person or remotely using [join.me](https://www.join.me/) or something similar. We appreciate you taking the time to complete this challenge. Our commitment to you is that if you submit a pull request, it will get a thorough review and we'll give you specific feedback even if you are not selected to pair with us.

## Help, I'm stuck.

We understand that as developer you may get stuck. That's ok. If you get stuck along the way, reach out. I'll be checking my [email](mailto:diego.desouza@anthem.com) regularly.
